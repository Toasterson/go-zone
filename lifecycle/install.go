package lifecycle

import (
	"git.wegmueller.it/illumos/go-zone/brand"
	"git.wegmueller.it/illumos/go-zone/config"
	"github.com/labstack/gommon/log"
	"github.com/ztrue/tracerr"
)

func install(manager Lifecyclemanager, args brand.HookOptions) error {
	z := manager.GetZone()
	b, err := brand.LoadBrand(manager.GetZoneBrand())
	if err != nil {
		return tracerr.Wrap(err)
	}
	for key, value := range getInternalOptions(manager) {
		args[key] = value
	}
	installCmd, err := b.GetHook("install", args)
	if err != nil {
		return tracerr.Wrap(err)
	}

	installCmd.SetOutput(manager.GetOutput())
	installCmd.SetErrorOut(manager.GetOutput())

	lock, err := config.GrabZoneLockFile(manager.GetZone())
	if err != nil {
		return tracerr.Wrap(err)
	}
	defer config.ReleaseZoneLockFile(lock)

	if err = createZfsZonepath(manager.GetZonePath()); err != nil {
		return tracerr.Wrap(err)
	}
	_, _ = manager.GetOutput().Write([]byte("A ZFS file system has been created for this zone.\n"))
	defer func() {
		if err != nil {
			if err = cleanupZonePath(manager.GetZonePath(), false); err != nil {
				log.Fatal(err)
			}
		}
	}()

	z.State = config.ZoneStateIncomplete
	if err = config.Update(z); err != nil {
		return tracerr.Wrap(err)
	}

	// Note that libzonecfg notifies via sysevent about state changes
	// but there seems to be no consumer in gate so we will leave it be
	// for now
	err = installCmd.Run()
	if err != nil {
		return tracerr.Wrap(err)
	}
	if postCmd, hookRetErr := b.GetHook("postinstall", nil); hookRetErr == nil {
		postCmd.SetOutput(manager.GetOutput())
		postCmd.SetErrorOut(manager.GetOutput())
		if hookRetErr = postCmd.Run(); hookRetErr != nil {
			err = hookRetErr
			return tracerr.Wrap(err)
		}
	}

	z.State = config.ZoneStateInstalled
	if err = config.Update(z); err != nil {
		return tracerr.Wrap(err)
	}

	//Re read Any changes made to the Index and XML after being done.
	return manager.UpdateZoneFromFile()
}
