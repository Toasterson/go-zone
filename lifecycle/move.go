package lifecycle

import (
	"github.com/ztrue/tracerr"
	"os/exec"
)

func move(m Lifecyclemanager, targetPath string) error {
	cmdArgs := []string{"-z", m.GetZoneName(), "move", targetPath}
	cmd := exec.Command(zoneadmCMD, cmdArgs...)
	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}
