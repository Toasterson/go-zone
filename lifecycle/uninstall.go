package lifecycle

import (
	"git.wegmueller.it/illumos/go-zone/brand"
	"git.wegmueller.it/illumos/go-zone/config"
)

func uninstall(manager *BrandedLifecycleManager, args brand.HookOptions) error {
	b, err := brand.LoadBrand(manager.zone.Brand)
	if err != nil {
		return err
	}
	for key, value := range getInternalOptions(manager) {
		args[key] = value
	}
	uninstallCmd, err := b.GetHook("uninstall", args)
	if err != nil {
		return err
	}

	preuninstallCmd, err := b.GetHook("preuninstall", args)
	if err != nil {
		return err
	}

	uninstallCmd.SetOutput(manager.output)
	uninstallCmd.SetErrorOut(manager.output)

	preuninstallCmd.SetOutput(manager.output)
	preuninstallCmd.SetErrorOut(manager.output)

	//TODO Ping Zoneadmd

	lock, err := config.GrabZoneLockFile(*manager.zone)
	if err != nil {
		return err
	}
	defer config.ReleaseZoneLockFile(lock)

	//TODO Check If there are mounts and abort if we do

	//TODO If we have a preuninstall hook run ist

	if err = preuninstallCmd.Run(); err != nil {
		return err
	}

	manager.zone.State = config.ZoneStateIncomplete
	if err = config.Update(*manager.zone); err != nil {
		return err
	}

	//TODO if we have a uninstall hook run it

	if err = uninstallCmd.Run(); err != nil {
		return err
	}

	//TODO else run some internal cleanup

	return nil

}
