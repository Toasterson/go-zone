package lifecycle

import (
	"github.com/ztrue/tracerr"
	"os/exec"
)

type CloneMethod int

const (
	CloneMethodAuto CloneMethod = 0
	CloneMethodCopy CloneMethod = 1
)

func clone(m Lifecyclemanager, method CloneMethod, snapshotName string, src string) error {
	cmdArgs := []string{"-z", m.GetZoneName(), "clone"}
	if method == CloneMethodCopy {
		cmdArgs = append(cmdArgs, "-m", "copy")
	}
	if snapshotName != "" {
		cmdArgs = append(cmdArgs, "-s", snapshotName)
	}
	cmdArgs = append(cmdArgs, src)
	cmd := exec.Command(zoneadmCMD, cmdArgs...)
	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}
