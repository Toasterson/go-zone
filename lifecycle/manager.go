package lifecycle

import (
	"fmt"
	"strings"
	"git.wegmueller.it/illumos/go-zone/brand"
	"git.wegmueller.it/illumos/go-zone/config"
	"github.com/ztrue/tracerr"
	"io"
	"os"
	"os/exec"
)

func NewManager(z *config.Zone) (Lifecyclemanager, error) {
	b, err := brand.LoadBrand(z.Brand)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	return &BrandedLifecycleManager{
		zone:   z,
		brand:  b,
		output: os.Stdout,
	}, nil
}

// Convinience Method for standard zoneadm behaviour.
func OpenZoneConfig(name string) (Lifecyclemanager, error) {
	z := config.New(name)
	err := z.ReadFromFile()
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	return NewManager(z)
}

type BrandedLifecycleManager struct {
	zone   *config.Zone
	brand  *brand.Brand
	output io.Writer
}

func (m *BrandedLifecycleManager) GetZoneName() string {
	return m.zone.Name
}

func (m *BrandedLifecycleManager) GetZoneBrand() string {
	return m.zone.Brand
}

func (m *BrandedLifecycleManager) GetZoneState() config.ZoneState {
	return m.zone.State
}

func (m *BrandedLifecycleManager) GetZoneId() config.ZoneId_T {
	return m.zone.Id
}

func (m *BrandedLifecycleManager) GetZonePath() string {
	return m.zone.Zonepath
}

func (m *BrandedLifecycleManager) GetOutput() io.Writer {
	return m.output
}

func (m *BrandedLifecycleManager) SetOutput(o io.Writer) {
	m.output = o
}

func (m *BrandedLifecycleManager) GetZone() config.Zone {
	return *m.zone
}

func (m *BrandedLifecycleManager) UpdateZoneFromFile() error {
	return m.zone.ReadFromFile()
}

func (m *BrandedLifecycleManager) GetAttribute(name string) (string, error) {
	attr := strings.ToLower(name)
	for _, v := range m.zone.Attributes {
		if strings.ToLower(v.Name) == attr {
			return v.Value, nil
		}
	}
	return "", fmt.Errorf("Attribute not found")
}

func (*BrandedLifecycleManager) SetAttribute(key, value string) error {
	panic("implement me")
}

func (m *BrandedLifecycleManager) Boot(args BootArguments) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "boot"); err != nil {
		return tracerr.Wrap(err)
	}
	if args == nil {
		args = make(BootArguments)
	}
	var cmd *exec.Cmd
	if len(args) > 0 {
		if !args.Valid() {
			return fmt.Errorf("invalid arguments in %+v", args)
		}
		cmdArgs := append([]string{"-z", m.zone.Name, "boot", "--"}, args.Unwrap()...)
		cmd = exec.Command(zoneadmCMD, cmdArgs...)
	} else {
		cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "boot")
	}
	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Halt() error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "halt"); err != nil {
		return tracerr.Wrap(err)
	}

	var cmd *exec.Cmd
	cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "halt")

	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (*BrandedLifecycleManager) Ready() error {
	panic("implement me")
}

func (m *BrandedLifecycleManager) Shutdown(args BootArguments) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "shutdown"); err != nil {
		return tracerr.Wrap(err)
	}

	var cmd *exec.Cmd
	cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "shutdown")

	if out, err := cmd.CombinedOutput(); err != nil {
		return tracerr.Wrap(fmt.Errorf("failed to run zoneadm -z %s shutdown: %s", m.zone.Name, out))
	}
	return nil
}

func (m *BrandedLifecycleManager) Reboot(args BootArguments) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "shutdown"); err != nil {
		return tracerr.Wrap(err)
	}
	if args == nil {
		args = make(BootArguments)
	}
	var cmd *exec.Cmd
	if len(args) > 0 {
		if !args.Valid() {
			return fmt.Errorf("invalid arguments in %+v", args)
		}
		cmdArgs := append([]string{"-z", m.zone.Name, "shutdown", "-r", "--"}, args.Unwrap()...)
		cmd = exec.Command(zoneadmCMD, cmdArgs...)
	} else {
		cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "shutdown", "-r")
	}
	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Verify() error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}

	var cmd *exec.Cmd
	cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "verify")

	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Install(args brand.HookOptions) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "install"); err != nil {
		return tracerr.Wrap(err)
	}
	if args == nil {
		args = make(brand.HookOptions)
	}
	if err := install(m, args); err != nil {
		m.zone.State = config.ZoneStateIncomplete
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Uninstall(force bool, args brand.HookOptions) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use uninstall in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "uninstall"); err != nil {
		return tracerr.Wrap(err)
	}
	if args == nil {
		args = make(brand.HookOptions)
	}
	if err := uninstall(m, args); err != nil {
		m.zone.State = config.ZoneStateIncomplete
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Clone(method CloneMethod, snapshotName string, src string) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use uninstall in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "clone"); err != nil {
		return tracerr.Wrap(err)
	}

	if src == "" {
		return fmt.Errorf("source is nil cannot clone into new zone %s", m.zone.Name)
	}

	if err := clone(m, method, snapshotName, src); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (m *BrandedLifecycleManager) Move(newZonepath string) error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use uninstall in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}
	if err := sanityChecks(m, "move"); err != nil {
		return tracerr.Wrap(err)
	}

	if newZonepath == "" {
		return fmt.Errorf("cannot move %s to unknown zonepath", m.zone.Name)
	}

	if err := move(m, newZonepath); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}

func (*BrandedLifecycleManager) Detach(noop bool) error {
	panic("implement me")
}

func (*BrandedLifecycleManager) Attach(force, noop bool, path string, args brand.HookOptions) error {
	panic("implement me")
}

func (m *BrandedLifecycleManager) MarkIncomplete() error {
	//Sanity Checks
	if config.IsAlternativeRoot() {
		return fmt.Errorf("cannot use install in alternative root dir")
	}
	if m.zone.Zonepath == "" {
		return fmt.Errorf("zonepath cannot be empty")
	}

	var cmd *exec.Cmd
	cmd = exec.Command(zoneadmCMD, "-z", m.zone.Name, "mark", "incomplete")

	if err := cmd.Run(); err != nil {
		return tracerr.Wrap(err)
	}
	return nil
}
