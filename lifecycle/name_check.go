package lifecycle

import "strings"

var reservedPrefixes = []string{
	"SUNW",
	"TRIB",
	"OI",
}

var reservedNames = []string{
	"global",
	"default",
}

func HasReservedPrefix(name string) bool {
	for _, pref := range reservedPrefixes {
		if strings.HasPrefix(name, pref) {
			return true
		}
	}
	return false
}

func IsNameReserved(name string) bool {
	for _, res := range reservedNames {
		if name == res {
			return true
		}
	}
	return false
}
