package lifecycle

import (
	"strings"
)

type BootArguments map[string]string

func (b BootArguments) Valid() bool {
	for key, val := range b {
		switch key {
		case "-i":
			if strings.Index(val, "/") == -1 {
				return false
			}
		case "-m":
		case "-s":
		default:
			return false
		}
	}
	return false
}

func (b BootArguments) Unwrap() []string {
	retVal := make([]string, 0)
	for key, val := range b {
		retVal = append(retVal, key, val)
	}
	return retVal
}
