package lifecycle

import (
	"fmt"

	"git.wegmueller.it/illumos/go-zone"
	"git.wegmueller.it/illumos/go-zone/config"
	"git.wegmueller.it/illumos/go-zone/privilege"
)

/*
 * Various sanity checks; make sure:
 * 1. We're in the global zone.
 * 2. The calling user has sufficient privilege.
 * 3. The target zone is neither the global zone nor anything starting with
 *    "SUNW".
 * 4a. If we're looking for a 'not running' (i.e., configured or installed)
 *     zone, the name service knows about it.
 * 4b. For some operations which expect a zone not to be running, that it is
 *     not already running (or ready).
 */

func sanityChecks(manager *BrandedLifecycleManager, command string) error {
	if zone.GetZoneId() != config.GlobalId {
		return fmt.Errorf("not in global zone")
	}
	privset := privilege.Allocate()
	if privset == nil {
		return fmt.Errorf("cannot allocate privilege set")
	}
	defer privilege.Free(privset)
	if ret, _ := privilege.ProcessContains(privset); ret == false {
		return fmt.Errorf("cannot get process privilege")
	}
	if !privilege.IsFull(privset) {
		return fmt.Errorf("only a privileged user may %s a zone", command)
	}
	if manager.zone.Name == "" {
		return fmt.Errorf("no zone specified")
	}
	//TODO auth_check

	if IsNameReserved(manager.zone.Name) {
		return fmt.Errorf("%s is a reserved name and cannot be used", manager.zone.Name)
	}
	if HasReservedPrefix(manager.zone.Name) {
		return fmt.Errorf("%s has a reserved prefix please use another name", manager.zone.Name)
	}

	//TODO Command specific checks (e.g. don't try to launch a running zone)
	//https://git.wegmueller.it/Illumos/illumos-joyent/src/branch/master/usr/src/cmd/zoneadm/zoneadm.c
	// line 1602

	switch command {
	case "clone":
		if manager.zone.State != config.ZoneStateInstalled {
			return fmt.Errorf("cannot clone a zone in state %s", manager.zone.State)
		}
	}

	return nil
}
