package lifecycle

import (
	"git.wegmueller.it/illumos/go-zone/brand"
)

func getInternalOptions(manager Lifecyclemanager) (args brand.HookOptions) {
	args = make(brand.HookOptions)
	args["zone"] = manager.GetZoneName()
	args["zonepath"] = manager.GetZonePath()
	args["zoneid"] = string(manager.GetZoneId())

	// Lx branded zones need an image to be created
	// such image could be a zss.gz or tar.gz file
	// that will be used by ZONEADM(1M).
	if string(manager.GetZoneBrand()) == "lx" {
		val, err := manager.GetAttribute("img")
		if err == nil {
			args["img"] = string(val)
		}
	}
	return args
}
