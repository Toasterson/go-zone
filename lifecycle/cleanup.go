package lifecycle

import (
	"fmt"
	"git.wegmueller.it/opencloud/opencloud/zfs"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Cleanup the ZonePath
// if all == false only the stdEntries are removed
// otherwise everything id removed
func cleanupZonePath(zonepath string, all bool) error {

	/*
	 * We shouldn't need these checks but lets be paranoid since we
	 * could blow away the whole system here if we got the wrong zonepath.
	 */
	if zonepath == "" || zonepath == "/" {
		return fmt.Errorf("invalid zonepath %s \n", zonepath)
	}

	/*
	 * If the dirpath is already gone (maybe it was manually removed) then
	 * we just return so that the cleanup is successful.
	 */
	if _, err := os.Stat(zonepath); os.IsNotExist(err) {
		return nil
	}

	if all {
		ds, err := zfs.OpenDatasetByPath(zonepath)
		if err != nil {
			return os.RemoveAll(zonepath)
		}
		return ds.Destroy(true)
	}
	entries, err := ioutil.ReadDir(zonepath)
	if err != nil {
		return err
	}
	for _, e := range entries {
		switch e.Name() {
		case "dev":
			fallthrough
		case "lastexited":
			fallthrough
		case "logs":
			fallthrough
		case "lu":
			fallthrough
		case "root":
			fallthrough
		case "SUNWattached.xml":
			os.RemoveAll(filepath.Join(zonepath, e.Name()))
		default:

		}
	}
	return nil
}
