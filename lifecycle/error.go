package lifecycle

import "fmt"

type ZoneErrorNumber int

/*
 * Exit values that may be returned by scripts or programs invoked by various
 * zone commands.
 *
 * These are defined as:
 *
 *      ZONE_SUBPROC_OK
 *      ===============
 *      The subprocess completed successfully.
 *
 *      ZONE_SUBPROC_USAGE
 *      ==================
 *      The subprocess failed with a usage message, or a usage message should
 *      be output in its behalf.
 *
 *      ZONE_SUBPROC_NOTCOMPLETE
 *      ========================
 *      The subprocess did not complete, but the actions performed by the
 *      subprocess require no recovery actions by the user.
 *
 *      For example, if the subprocess were called by "zoneadm install," the
 *      installation of the zone did not succeed but the user need not perform
 *      a "zoneadm uninstall" before attempting another install.
 *
 *      ZONE_SUBPROC_FATAL
 *      ==================
 *      The subprocess failed in a fatal manner, usually one that will require
 *      some type of recovery action by the user.
 *
 *      For example, if the subprocess were called by "zoneadm install," the
 *      installation of the zone did not succeed and the user will need to
 *      perform a "zoneadm uninstall" before another install attempt is
 *      possible.
 *
 *      The non-success exit values are large to avoid accidental collision
 *      with values used internally by some commands (e.g. "Z_ERR" and
 *      "Z_USAGE" as used by zoneadm.)
 */
const (
	ZONE_SUBPROC_OK          ZoneErrorNumber = 0
	ZONE_SUBPROC_USAGE                       = 253
	ZONE_SUBPROC_NOTCOMPLETE                 = 254
	ZONE_SUBPROC_FATAL                       = 255
)

type ZoneError struct {
	ErrNo   ZoneErrorNumber
	Message string
}

func (z *ZoneError) Error() string {
	return fmt.Sprintf("Error %d occured: %s", z.ErrNo, z.Message)
}

func (z *ZoneError) RuntimeError() {}
