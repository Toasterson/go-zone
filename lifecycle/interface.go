package lifecycle

import (
	"git.wegmueller.it/illumos/go-zone/brand"
	"git.wegmueller.it/illumos/go-zone/config"
	"io"
)

// A Lifecycle Manager managed the Lifecycle of a zone.
// that means it executes the Installation Code
// launches the associated RuntimeManager and orders it around
// And executes everything needed for installing/removing a zone from the system
// A zone Configuration must be passed to the manager beforehand.
type Lifecyclemanager interface {
	GetZoneId() config.ZoneId_T
	GetZonePath() string
	GetZoneName() string
	GetZoneBrand() string
	GetZone() config.Zone
	UpdateZoneFromFile() error
	GetOutput() io.Writer
	GetZoneState() config.ZoneState
	GetAttribute(name string) (string, error)
	SetAttribute(key, value string) error
	Boot(args BootArguments) error
	Halt() error
	Ready() error
	Shutdown(args BootArguments) error
	Reboot(args BootArguments) error
	Verify() error
	Install(args brand.HookOptions) error
	Uninstall(force bool, args brand.HookOptions) error
	Clone(method CloneMethod, snapshotName string, src string) error
	Move(newZonepath string) error
	Detach(noop bool) error
	Attach(force, noop bool, path string, args brand.HookOptions) error
	MarkIncomplete() error
	SetOutput(io.Writer)
}
