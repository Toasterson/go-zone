package lifecycle

import (
	"fmt"
	"os"
	"path/filepath"

	"git.wegmueller.it/opencloud/opencloud/zfs"
)

func path2name(zonepath string) (fsName string, err error) {
	dname, bname := filepath.Split(zonepath)
	dname = dname[:len(dname)-1]
	/*
	 * This is a quick test to save iterating over all of the zfs datasets
	 * on the system (which can be a lot).  If the parent dir is not in a
	 * ZFS fs, then we're done.
	 */
	var stat os.FileInfo
	stat, err = os.Stat(dname)
	if err != nil || !stat.IsDir() {
		if err == nil {
			err = fmt.Errorf("zonepath parent not directory")
		}
		return "", err
	}
	var parentDs *zfs.Dataset
	if parentDs, err = zfs.OpenDatasetByPath(dname); err != nil {
		return "", err
	}
	return fmt.Sprintf("%s/%s", parentDs.Path, bname), nil
}

func createZfsZonepath(zonepath string) error {
	zfsName, err := path2name(zonepath)
	if err != nil {
		return err
	}
	_, err = zfs.CreateDataset(zfsName, zfs.DatasetTypeFilesystem, zfs.Properties{
		"sharesmb": "off",
		"sharenfs": "off",
	})
	if err != nil {
		return err
	}

	if err = os.Chmod(zonepath, 0700); err != nil {
		return err
	}
	return nil
}
