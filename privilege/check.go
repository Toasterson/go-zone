package privilege

/*
#include <priv.h>
*/
import "C"

func IsFull(p *C.priv_set_t) bool {
	if C.priv_isfullset(p) == 0 {
		return false
	}
	return true
}
