package privilege

/*
#include <priv.h>
*/
import "C"

// Allocate a priviledge set
func Allocate() *C.priv_set_t {
	return C.priv_allocset()
}

func Free(p *C.priv_set_t) {
	C.priv_freeset(p)
}
