package privilege

/*
#include <priv.h>
#include <sys/priv_const.h>
*/
import "C"
import (
	"fmt"
	"golang.org/x/sys/unix"
	"syscall"
)

func ProcessContains(privSet *C.priv_set_t) (bool, error) {
	if retVal := C.getppriv(C.PRIV_EFFECTIVE, privSet); retVal != 0 {
		if syscall.Errno(retVal) == unix.EINVAL {
			return false, fmt.Errorf("out of range")
		} else if syscall.Errno(retVal) == unix.EFAULT {
			return false, fmt.Errorf("set point to illegal address")
		}
		return false, nil
	}
	return true, nil
}
