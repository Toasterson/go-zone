package zone

/*
#include <zone.h>
#include <stdlib.h>
*/
import "C"

import (
	"fmt"
	"unsafe"

	"git.wegmueller.it/illumos/go-zone/config"
)

func getattr(id config.ZoneId_T, attr int32, value *string, length int32) error {
	if length == 0 {
		length = config.MAXPATHLEN
	}
	buffer := C.malloc((C.ulong)(length))
	defer C.free(unsafe.Pointer(buffer))
	size := C.zone_getattr((C.int)(id), (C.int)(attr), unsafe.Pointer(buffer), (C.ulong)(length))
	if size == 0 {
		return fmt.Errorf("could not get %d Attr from zone %d", attr, id)
	}
	*value = C.GoString((*C.char)(buffer))
	return nil
}

func getzonenamebyid(id config.ZoneId_T) string {
	buffer := C.malloc((C.ulong)(config.ZONENAME_MAX))
	C.getzonenamebyid((C.zoneid_t)(id), (*C.char)(buffer), (C.size_t)(config.ZONENAME_MAX))
	defer C.free(unsafe.Pointer(buffer))
	return C.GoString((*C.char)(buffer))
}

func lookup(name string) config.ZoneId_T {
	rval := C.getzoneidbyname(C.CString(name))
	return config.ZoneId_T(rval)
}

func getzoneid() config.ZoneId_T {
	return config.ZoneId_T(C.getzoneid())
}
