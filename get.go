//+build solaris

package zone

import "git.wegmueller.it/illumos/go-zone/config"

func GetZoneId() config.ZoneId_T {
	return getzoneid()
}

func GetZoneIdByName(name string) (id config.ZoneId_T) {
	return lookup(name)
}

func GetZoneNameById(id config.ZoneId_T) (name string) {
	return getzonenamebyid(id)
}

func GetAttr(id config.ZoneId_T, attrnum int32) string {
	var length int32
	value := ""
	switch attrnum {
	case config.ZONE_ATTR_ROOT:
		length = config.MAXPATHLEN
	case config.ZONE_ATTR_NAME:
		length = config.MAXPATHLEN
	case config.ZONE_ATTR_BRAND:
		length = config.MAXNAMELEN
	case config.ZONE_ATTR_UNIQID:
		length = config.UUID_PRINTABLE_STRING_LENGTH
	default:
		length = config.MAXPATHLEN
	}
	if err := getattr(id, attrnum, &value, length); err != nil {
		return ""
	}
	return value
}
