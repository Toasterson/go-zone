package zone

import (
	"os/exec"

	"git.wegmueller.it/illumos/go-zone/config"
)

const zloginBin = "zlogin"

// Command Wraps zlogin to properly execute a command inside the zone
func Command(zone config.Zone, user string, cmd string, args ...string) (*exec.Cmd, error) {
	zloginPath, err := exec.LookPath(zloginBin)
	if err != nil {
		return nil, err
	}

	var arguments []string
	if user != "" {
		arguments = []string{"-l", user}
	} else {
		arguments = make([]string, 0)
	}

	arguments = append(arguments, zone.Name, cmd)

	arguments = append(arguments, args...)

	return exec.Command(zloginPath, arguments...), nil
}

// Execute executes cmd inside zone as user with arguments given
func ExecuteInside(zone config.Zone, user string, cmd string, args ...string) error {
	command, err := Command(zone, user, cmd, args...)
	if err != nil {
		return err
	}

	return command.Run()
}
