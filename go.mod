module git.wegmueller.it/illumos/go-zone

go 1.11

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20180906201452-2aa6f33b730c
	git.wegmueller.it/opencloud/opencloud v0.0.1
	github.com/labstack/gommon v0.2.8
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/ztrue/tracerr v0.3.0
	golang.org/x/sys v0.0.0-20190415081028-16da32be82c5
)
