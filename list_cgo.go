//+build solaris

package zone

/*
#include <zone.h>
*/
import "C"

import (
	"fmt"
	"unsafe"

	"git.wegmueller.it/illumos/go-zone/config"
)

func list() (zids []config.ZoneId_T, err error) {
	var nzents config.Uint_T = 0
	var nzentsSaved config.Uint_T = 0

	e1 := C.zone_list(nil, (*C.uint)(&nzents))
	if e1 != 0 {
		return nil, fmt.Errorf("could not get numer of running zones")
	}

again:
	zids = make([]config.ZoneId_T, nzents)
	nzentsSaved = nzents
	e2 := C.zone_list((*C.int)(unsafe.Pointer(&zids[0])), (*C.uint)(&nzents))
	if e2 != 0 {
		return nil, fmt.Errorf("could not get list of running zones")
	}

	if nzents != nzentsSaved {
		goto again
	}

	return
}
