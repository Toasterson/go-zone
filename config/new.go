package config

func New(name string) *Zone {
	return &Zone{
		Name:             name,
		Zonepath:         "",
		State:            ZoneStateConfigured,
		Autoboot:         false,
		Brand:            "",
		IpType:           IpTypeShared,
		Privileges:       make(Privileges, 0),
		FileSystems:      make([]FileSystem, 0),
		Networks:         make([]Network, 0),
		Devices:          make([]Device, 0),
		ResourceControls: make([]ResourceControl, 0),
		Attributes:       make([]Attribute, 0),
		Datasets:         make([]Dataset, 0),
		SecurityFlags:    make([]SecurityFlags, 0),
		Admin:            make([]Admin, 0),
	}
}

func NewFromDefault(name string) (*Zone, error) {
	z := New(DefaultZoneName)
	if err := z.ReadFromFile(); err != nil {
		return nil, err
	}
	z.Name = name
	return z, nil
}

func NewFromTemplate(name, template string) (*Zone, error) {
	z := New(template)
	if err := z.ReadFromFile(); err != nil {
		return nil, err
	}
	z.Name = name
	return z, nil
}
