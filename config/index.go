package config

import (
	"bufio"
	"fmt"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"os"
	"path/filepath"
	"strings"
)

func readIndex() (header string, zones []string, err error) {
	buffer := make([]string, 0)
	var lockFd *os.File
	if lockFd, err = LockIndex(); err != nil {
		return "", nil, err
	}
	defer Unlock(lockFd)

	f, err := os.Open(indexPath)
	if err != nil {
		return "", nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		buffer = append(buffer, scanner.Text())
	}
	if err = scanner.Err(); err != nil {
		return "", nil, err
	}
	zones = make([]string, 0)
	headers := make([]string, 0)
	for _, line := range buffer {
		if strings.HasPrefix(line, "#") {
			headers = append(headers, line)
		} else if line != "" && line != "\n" && line != " " {
			zones = append(zones, line)
		}
	}
	header = strings.Join(headers, "\n") + "\n"
	return header, zones, nil
}

func writeIndex(header string, zones []string) (err error) {
	var lockFd *os.File
	if lockFd, err = LockIndex(); err != nil {
		return err
	}
	defer Unlock(lockFd)

	f, err := os.Create(indexPath + ".new")
	if err != nil {
		return err
	}
	defer f.Close()

	indexFile := header
	indexFile += strings.Join(zones, "\n")
	indexFile += "\n"
	if _, err = f.WriteString(indexFile); err != nil {
		return err
	}
	return os.Rename(indexPath+".new", indexPath)
}

func Register(z *Zone) (err error) {
	if z.Name == "" || z.Zonepath == "" {
		return fmt.Errorf("cannot register zone without zonepath or name abort")
	}
	if _, err := os.Stat(filepath.Join(Path, z.Name+".xml")); err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("xml config for %s does not exist please create first", z.Name)
		} else {
			return err
		}
	}

	header, zones, err := readIndex()
	if err != nil {
		return err
	}
	for _, zLine := range zones {
		if strings.HasPrefix(zLine, z.Name) {
			return fmt.Errorf("zone %s already registered", z.Name)
		}
	}
	//I think this should never happen as an uninitialized is 0 but it does not hurt
	if z.State <= ZoneStateConfigured {
		z.State = ZoneStateConfigured
	}
	uuidStr := ""
	if z.State > ZoneStateIncomplete {
		if z.UUID == uuid.Nil {
			z.UUID = uuid.NewV4()
		}
		uuidStr = z.UUID.String()
	}
	newZline := fmt.Sprintf("%s:%s:%s:", z.Name, getZoneStateName(z.State), z.Zonepath)
	if uuidStr != "" {
		newZline += ":" + uuidStr
	}
	zones = append(zones, newZline)
	return writeIndex(header, zones)
}

func Get(name string) (state, path, uuid string, err error) {
	_, zones, err := readIndex()
	if err != nil {
		return "", "", "", err
	}
	for _, line := range zones {
		if strings.HasPrefix(line, name) {
			parsed := strings.Split(line, ":")
			uuidStr := ""
			if len(parsed) == 4 {
				uuidStr = parsed[3]
			}
			return parsed[1], parsed[2], uuidStr, nil
		}
	}
	return "", "", "", errors.New("zone not found")
}

func Update(z Zone) error {
	headers, zones, err := readIndex()
	if err != nil {
		return err
	}
	uuidStr := ""
	//Make sure we have a uuid if we modify the state to installed or above
	if z.State > ZoneStateIncomplete {
		if z.UUID == uuid.Nil {
			z.UUID = uuid.NewV4()
		}
		uuidStr = z.UUID.String()
	}

	for i, zLine := range zones {
		if strings.HasPrefix(zLine, z.Name) {
			parsed := strings.Split(zLine, ":")
			if len(parsed) == 4 {
				uuidStr = parsed[3]
			}
			if getZoneStateName(z.State) == parsed[1] && parsed[2] == z.Zonepath {
				if (uuidStr == "" && z.UUID.String() == "") || (uuidStr == z.UUID.String()) {
					return fmt.Errorf("no cahnges to %s detected skipping", z.Name)
				}
			}
			newZline := fmt.Sprintf("%s:%s:%s:", z.Name, getZoneStateName(z.State), z.Zonepath)
			if uuidStr != "" {
				newZline += ":" + uuidStr
			}
			zones[i] = newZline
		}
	}

	return writeIndex(headers, zones)
}

func LockIndex() (lockFd *os.File, err error) {
	if err = os.Mkdir(lockDir, 0700); !os.IsExist(err) {
		return nil, err
	}
	return Lock(indexLockPath)
}

func Unregister(z *Zone) (err error) {
	headers, zones, err := readIndex()
	if err != nil {
		return err
	}
	modified := false
	newZones := make([]string, 0)
	for _, zLine := range zones {
		if !strings.HasPrefix(zLine, z.Name) {
			newZones = append(newZones, zLine)
		} else {
			modified = true
		}
	}
	if modified {
		return writeIndex(headers, newZones)
	}
	return nil
}
