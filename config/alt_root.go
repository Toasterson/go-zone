package config

var rootDir = "/"

func SetRoot(newRoot string) {
	rootDir = newRoot
}

func GetRoot() string {
	return rootDir
}

func IsAlternativeRoot() bool {
	return rootDir != "/"
}
