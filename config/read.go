package config

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"os"
	"path/filepath"
)

func (z *Zone) Read(blob []byte) error {
	return xml.NewDecoder(bytes.NewBuffer(blob)).Decode(z)
}

func (z *Zone) ReadFromFile() error {
	if z.Name == "" {
		return fmt.Errorf("zone name must not be empty")
	}
	if z.Name == "global" {
		return nil
	}
	path := filepath.Join(Path, z.Name+".xml")
	file, err := os.Open(path)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("cannot read zone config for %s", z.Name))
	}
	defer file.Close()
	state, zpath, uuidStr, err := Get(z.Name)
	if err != nil {
		return err
	}
	z.State = getZoneState(state)
	z.Zonepath = zpath
	z.UUID = uuid.FromStringOrNil(uuidStr)
	return xml.NewDecoder(file).Decode(z)
}
