package config

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"syscall"
)

var localHaveLock bool

func GrabZoneLockFile(z Zone) (lockFd *os.File, err error) {
	/*
	 * If we already have the lock, we can skip this expensive song
	 * and dance.
	 */
	if localHaveLock {
		return nil, errors.New("already have zone lock")
	}
	lockPath := filepath.Join(GetRoot(), tmpDir)
	if err = os.Mkdir(lockPath, 0700); !os.IsExist(err) {
		return nil, err
	}
	/*
	 * One of these lock files is created for each zone (when needed).
	 */
	lockPath = fmt.Sprintf("%s/%s.zoneadm.lock", lockPath, z.Name)
	localHaveLock = true
	return Lock(lockPath)
}

func ReleaseZoneLockFile(fd *os.File) error {
	if err := Unlock(fd); err != nil {
		// If we have a case where we cannot unlock the lock then the whole
		// application should fail. So do not unset the local Blocker
		return err
	}
	localHaveLock = false
	return nil
}

func Lock(name string) (lockFd *os.File, err error) {
	lockFd, err = os.OpenFile(name, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}
	lock := syscall.Flock_t{
		Type:   syscall.F_WRLCK,
		Whence: io.SeekStart,
		Start:  0,
		Len:    0,
	}
	if err = syscall.FcntlFlock(lockFd.Fd(), syscall.F_SETLKW, &lock); err != nil {
		lockFd.Close()
		if err == syscall.EAGAIN {
			err = errors.New("already locked")
		}
		return nil, err
	}
	return lockFd, nil
}

func Unlock(fd *os.File) (err error) {
	lock := syscall.Flock_t{
		Type:   syscall.F_UNLCK,
		Whence: io.SeekStart,
		Start:  0,
		Len:    0,
	}
	if err = syscall.FcntlFlock(fd.Fd(), syscall.F_SETLK, &lock); err != nil {
		return err
	}
	return fd.Close()
}
