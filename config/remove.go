package config

import (
	"os"
	"path/filepath"
)

func (z *Zone) RemoveFile() (err error) {
	path := filepath.Join(Path, z.Name+".xml")
	return os.Remove(path)
}
