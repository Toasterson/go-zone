// +build ignore

package config

/*
#include <zone.h>
#include <sys/zone.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <libzonecfg.h>
*/
import "C"

type Size_T C.size_t
type ZoneId_T C.zoneid_t
type Uint_T C.uint_t
type Uint32_T C.uint32_t

type ZoneState C.zone_status_t

type IpType C.zone_iptype_t

type Cmd_T C.zone_cmd_t

type CmdArg C.zone_cmd_arg_t

type CmdRval C.zone_cmd_rval_t
