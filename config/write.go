package config

import (
	"bytes"
	"encoding/xml"
	"github.com/pkg/errors"
	"io"
	"os"
	"path/filepath"
)

func (z *Zone) Write() (blob []byte, err error) {
	buff := bytes.NewBuffer(nil)
	e := xml.NewEncoder(buff)
	e.Indent("", "\t")
	if err = e.Encode(z); err != nil {
		return nil, err
	}
	buff.WriteString("\n")
	return buff.Bytes(), nil
}

func (z *Zone) WriteToFile() (err error) {
	path := filepath.Join(Path, z.Name+".xml")
	tmpFile, err := os.Create(filepath.Join(Path, "new_"+z.Name))
	if err != nil {
		return errors.Wrap(err, "cannot create tempfile")
	}
	defer func() {
		tmpFile.Close()
		os.Remove(tmpFile.Name())
	}()
	blob, err := z.Write()
	if err != nil {
		return errors.Wrap(err, "cannot serialize "+z.Name)
	}
	if _, err = writeHeader(tmpFile); err != nil {
		return err
	}
	if _, err = tmpFile.Write(blob); err != nil {
		return err
	}
	os.Remove(path)
	return os.Rename(tmpFile.Name(), path)
}

func writeHeader(writer io.Writer) (int, error) {
	return writer.Write([]byte(XMLHeader))
}
