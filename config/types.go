// Created by cgo -godefs - DO NOT EDIT
// cgo -godefs ctypes.go

package config

type Size_T uint64
type ZoneId_T int32
type Uint_T uint32
type Uint32_T uint32

type ZoneState uint32

/*
type ZoneDef struct {
	Zone_name      *int8
	Zone_root      *int8
	Zone_privs     *C.priv_set_t
	Zone_privssz   uint64
	Rctlbuf        *int8
	Rctlbufsz      uint64
	Extended_error *int32
	Zfsbuf         *int8
	Zfsbufsz       uint64
	Match          int32
	Doi            uint32
	Label          *struct{}
	Flags          int32
	Pad_cgo_0      [4]byte
}

*/

type IpType uint32

type Cmd_T uint32

type CmdArg struct {
	Uniqid  uint64
	Cmd     uint32
	X_pad   uint32
	Locale  [1024]int8
	Bootbuf [256]int8
}

type CmdRval struct {
	Rval      int32
	Errbuf    [1]int8
	Pad_cgo_0 [3]byte
}
