package config

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"strings"

	"code.cloudfoundry.org/bytefmt"
	"github.com/satori/go.uuid"
)

type Privileges []string

func (p Privileges) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	return xml.Attr{Name: name, Value: strings.Join(p, ",")}, nil
}

func (p *Privileges) UnmarshalXMLAttr(attr xml.Attr) error {
	*p = strings.Split(attr.Value, ",")
	return nil
}

const (
	IpTypeShared    IpType = 1
	IpTypeExclusive        = 2
)

func (i *IpType) UnmarshalXMLAttr(attr xml.Attr) error {
	switch attr.Value {
	case "exclusive":
		*i = IpTypeExclusive
	default:
		*i = IpTypeShared
	}
	return nil
}

func (i IpType) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	value := "shared"
	if i == IpTypeExclusive {
		value = "exclusive"
	}
	return xml.Attr{Name: name, Value: value}, nil
}

type Autoboot bool

func (a *Autoboot) UnmarshalXMLAttr(attr xml.Attr) error {
	*a = false
	if attr.Value == "true" {
		*a = true
	}
	return nil
}

func (a Autoboot) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	v := "false"
	if a {
		v = "true"
	}
	return xml.Attr{Name: name, Value: v}, nil
}

type Zone struct {
	Id               ZoneId_T          `xml:"-"`
	UUID             uuid.UUID         `xml:"-"`
	XMLName          xml.Name          `xml:"zone"`
	State            ZoneState         `xml:"-"`
	Name             string            `xml:"name,attr"`
	Zonepath         string            `xml:"zonepath,attr"`
	Autoboot         Autoboot          `xml:"autoboot,attr"`
	Bootargs         string            `xml:"bootargs,attr,omitempty"`
	ResourcePool     string            `xml:"pool,attr,omitempty"`
	Brand            string            `xml:"brand,attr"`
	IpType           IpType            `xml:"ip-type,attr"`
	Privileges       Privileges        `xml:"limitpriv,attr,omitempty"`
	HostId           string            `xml:"hostid,attr,omitempty"`
	SchedulingClass  string            `xml:"scheduling-class,attr,omitempty"`
	FSAllowed        string            `xml:"fs-allowed,attr,omitempty"`
	FileSystems      []FileSystem      `xml:"filesystem,omitempty"`
	Networks         []Network         `xml:"network"`
	Devices          []Device          `xml:"device,omitempty"`
	ResourceControls []ResourceControl `xml:"rctl,omitempty"`
	Attributes       []Attribute       `xml:"attr,omitempty"`
	Datasets         []Dataset         `xml:"dataset,omitempty"`
	DedicatedCpu     *DedicatedCpu     `xml:"pset,omitempty"`
	CappedMemory     *MemoryCap        `xml:"mcap,omitempty"`
	SecurityFlags    []SecurityFlags   `xml:"security-flags,omitempty"`
	Admin            []Admin           `xml:"admin,omitempty"`
}

func (z *Zone) SetResourceControl(name, privilege, limit, action string) {
	for i, r := range z.ResourceControls {
		if r.Name == name {
			r.Value.Privilege = privilege
			r.Value.Limit = limit
			r.Value.Action = action
			z.ResourceControls[i] = r
			return
		}
	}
	z.ResourceControls = append(z.ResourceControls, ResourceControl{
		Name: name,
		Value: ResourceControlValue{
			Privilege: privilege,
			Limit:     limit,
			Action:    action,
		},
	})
}

func (z *Zone) RemoveResourceControl(name string) {
	for i, r := range z.ResourceControls {
		if r.Name == name {
			z.ResourceControls = append(z.ResourceControls[:i], z.ResourceControls[i+1:]...)
		}
	}
}

func (z *Zone) SetCPUShares(shares string) {
	z.SetResourceControl("zone.cpu-shares", "privileged", shares, "none")
}

func (z *Zone) RemoveCPUShares() {
	z.RemoveResourceControl("zone.cpu-shares")
}

func (z *Zone) SetMaxLwps(lwps string) {
	z.SetResourceControl("zone.max-lwps", "privileged", lwps, "deny")
}

func (z *Zone) RemoveMaxLwps() {
	z.RemoveResourceControl("zone.max-lwps")
}

func (z *Zone) SetMaxMsgIds(maxIds string) {
	z.SetResourceControl("zone.max-msg-ids", "privileged", maxIds, "deny")
}

func (z *Zone) RemoveMaxMsgIds() {
	z.RemoveResourceControl("zone.max-msg-ids")
}

func (z *Zone) SetMaxSemIds(maxIds string) {
	z.SetResourceControl("zone.max-sem-ids", "privileged", maxIds, "deny")
}

func (z *Zone) RemoveMaxSemIds() {
	z.RemoveResourceControl("zone.max-sem-ids")
}

func (z *Zone) SetMaxShmIds(maxIds string) {
	z.SetResourceControl("zone.max-shm-ids", "privileged", maxIds, "deny")
}

func (z *Zone) RemoveMaxShmIds() {
	z.RemoveResourceControl("zone.max-shm-ids")
}

func (z *Zone) SetMaxShmMemory(memory string) {
	b, err := bytefmt.ToBytes(memory)
	if err != nil {
		return
	}
	z.SetResourceControl("zone.max-shm-memory", "privileged", strconv.FormatUint(b, 10), "deny")
}

func (z *Zone) RemoveMaxShmMemory() {
	z.RemoveResourceControl("zone.max-shm-memory")
}

func (z *Zone) SetMaxLockedMemory(memory string) {
	b, err := bytefmt.ToBytes(memory)
	if err != nil {
		return
	}
	z.SetResourceControl("zone.max-locked-memory", "privileged", strconv.FormatUint(b, 10), "deny")
}

func (z *Zone) RemoveMaxLockedMemory() {
	z.RemoveResourceControl("zone.max-locked-memory")
}

func (z *Zone) SetMaxSwap(memory string) {
	b, err := bytefmt.ToBytes(memory)
	if err != nil {
		return
	}
	z.SetResourceControl("zone.max-swap", "privileged", strconv.FormatUint(b, 10), "deny")
}

func (z *Zone) RemoveMaxSwap() {
	z.RemoveResourceControl("zone.max-swap")
}

func (z *Zone) SetCappedCPU(cap float64) {
	capStr := strconv.FormatFloat(cap*100, 'f', 0, 64)
	z.SetResourceControl("zone.cpu-cap", "privileged", capStr, "deny")
}

func (z *Zone) RemoveCappedCPU() {
	z.RemoveResourceControl("zone.cpu-cap")
}

type ResourceControl struct {
	Name  string               `xml:"name,attr"`
	Value ResourceControlValue `xml:"rctl-value"`
}

type ResourceControlValue struct {
	Privilege string `xml:"priv,attr"`
	Limit     string `xml:"limit,attr"`
	Action    string `xml:"action,attr"`
}

type MemoryCap struct {
	PhysicalCap uint64 `xml:"physcap,attr"`
}

func (m MemoryCap) String() string {
	return bytefmt.ByteSize(m.PhysicalCap)
}

func NewMemoryCap(memory string) *MemoryCap {
	b, err := bytefmt.ToBytes(memory)
	if err != nil {
		return nil
	}
	return &MemoryCap{
		PhysicalCap: b,
	}
}

type FSoption struct {
	Name string `xml:"name,attr"`
}

type FileSystem struct {
	Dir     string     `xml:"directory,attr"`
	Special string     `xml:"special,attr"`
	Raw     string     `xml:"raw,attr,omitempty"`
	Type    string     `xml:"type,attr"`
	Fsoption []FSoption `xml:"fsoption,omitempty"`
}

type Network struct {
	Address        string `xml:"address,attr,omitempty"`
	AllowedAddress string `xml:"allowed-address,attr,omitempty"`
	Physical       string `xml:"physical,attr"`
	Defrouter      string `xml:"defrouter,attr,omitempty"`
}

type Device struct {
	Match string `xml:"match,attr"`
}

type Attribute struct {
	Name  string `xml:"name,attr"`
	Type  string `xml:"type,attr"`
	Value string `xml:"value,attr"`
}

type Dataset struct {
	Name string `xml:"name,attr"`
}

// Note: Importance Property is not yet Supported
type DedicatedCpu struct {
	NcpuMin int `xml:"ncpu_min,attr"`
	NcpuMax int `xml:"ncpu_max,attr"`
}

func NewDedicatedCPU(exp string) (*DedicatedCpu, error) {
	var min, max int
	var err error
	if idx := strings.Index(exp, "-"); idx == -1 {
		min, err = strconv.Atoi(exp)
		if err != nil {
			return nil, fmt.Errorf("exp must be integer of range seperated by - char: %v", err)
		}
		max = min
	} else {
		min, err = strconv.Atoi(exp[:idx])
		if err != nil {
			return nil, fmt.Errorf("exp must be integer of range seperated by - char: %v", err)
		}
		min, err = strconv.Atoi(exp[idx+1:])
		if err != nil {
			return nil, fmt.Errorf("exp must be integer of range seperated by - char: %v", err)
		}
	}
	return &DedicatedCpu{NcpuMin: min, NcpuMax: max}, nil
}

func (d *DedicatedCpu) String() string {
	if d.NcpuMax == d.NcpuMin {
		return strconv.Itoa(d.NcpuMin)
	}
	return fmt.Sprintf("%d-%d", d.NcpuMin, d.NcpuMax)
}

type SecurityFlags struct {
	Lower   string `xml:"lower,attr"`
	Default string `xml:"default,attr"`
	Upper   string `xml:"upper,attr"`
}

type Admin struct {
	User  string `xml:"user,attr"`
	Auths string `xml:"auths,attr"`
}
