package brand

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"git.wegmueller.it/illumos/go-zone/config"
)

type HookOptions map[string]string

type Hook struct {
	options HookOptions
	command string
	root    string
	stdout  io.Writer //capture the output to somewhere
	stdin   io.Reader //Allows to pipe arbitrary input into the handler
	stderr  io.Writer //capture errors to somewhere
}

func (h *Hook) SetOutput(writer io.Writer) {
	h.stdout = writer
}

func (h *Hook) SetInput(reader io.Reader) {
	h.stdin = reader
}

func (h *Hook) SetErrorOut(writer io.Writer) {
	h.stderr = writer
}

func (h *Hook) IsEmpty() bool {
	return h.command == ""
}

// Get the Hook listed in the Brand
// Validate the options against the allowed ones
func (b *Brand) GetHook(name string, opts HookOptions) (*Hook, error) {
	switch name {
	case "install":
		if validateHookOptions(b.InstallOpts, opts) {
			return newHook(b.Install, opts), nil
		}
		return nil, fmt.Errorf("invalid option in %+v: allowed are %s", opts, b.InstallOpts)
	case "postinstall":
		if b.PostInstall == "" {
			return nil, fmt.Errorf("no postinstall command")
		}
		return newHook(b.PostInstall, opts), nil
	case "attach":
		return newHook(b.Attach, opts), nil
	case "detach":
		return newHook(b.Detach, opts), nil
	case "clone":
		return newHook(b.Clone, opts), nil
	case "uninstall":
		return newHook(b.Uninstall, opts), nil
	case "preuninstall":
		return newHook(b.PreUninstall, opts), nil
	case "prestatechange":
		return newHook(b.PreStateChange, opts), nil
	case "poststatechange":
		return newHook(b.PostStateChange, opts), nil
	case "query":
		return newHook(b.Query, opts), nil
	case "postclone":
		if b.Postclone == "" {
			return nil, fmt.Errorf("no postclone hook")
		}
		return newHook(b.Postclone, opts), nil
	default:
		return nil, fmt.Errorf("unknown brand hook")
	}
}

func newHook(cmd string, opts HookOptions) *Hook {
	hook := &Hook{
		options: opts,
		command: cmd,
		root:    config.GetRoot(),
		stdin:   os.Stdin,
		stdout:  os.Stdout,
		stderr:  os.Stderr,
	}
	return hook
}

// Run the Hook
// Report an error if we have no zone but require arguments from that struct
func (h *Hook) Run() error {

	if h.IsEmpty() {
		return nil
	}

	// a % in the Command means we need some arguments from the options struct
	if strings.Contains(h.command, "%") {
		for _, str := range strings.Split(h.command, " ") {
			if strings.Contains(str, "%") {
				if err := h.replaceOptInCommand(str); err != nil {
					return err
				}
			}
		}
	}
	// the image is specified using one of these flags:
	// -t : if the image is a tar.gz file
	// -s : if the image is a zss file
	if val, ok := h.options["img"]; ok {
		if strings.Contains(h.options["img"], ".zss.gz") {
			h.command = h.command + " -s " + val
		} else {
			h.command = h.command + " -t " + val
		}
	}

	cmdLine := strings.Split(h.command, " ")
	cmd := exec.Command(cmdLine[0], cmdLine[1:]...)

	// Let the hook output to somewhere
	cmd.Stdin = h.stdin
	cmd.Stdout = h.stdout
	cmd.Stderr = h.stderr
	return cmd.Run()
}

func (h *Hook) replaceOptInCommand(opt string) error {
	optKey := ""
	switch opt {
	case "%z":
		optKey = "zone"
	case "%R":
		optKey = "zonepath"
	case "%u":
		optKey = "username"
	case "%Z":
		optKey = "zoneid"
	default:
		return fmt.Errorf("option %s is unknown", opt)
	}
	optVal, ok := h.options[optKey]
	if !ok {
		return fmt.Errorf("option %s is not in options", opt)
	}
	h.command = strings.Replace(h.command, opt, optVal, -1)

	// Unset the value in options so we can ignore it when appending the rest of the arguments
	h.options[optKey] = ""
	return nil
}

func validateHookOptions(valid string, opts HookOptions) bool {
	validOpts := strings.Split(valid, ":")
	validOpts = append(validOpts, "zone", "zonepath", "username", "zoneid", "img")
	for key := range opts {
		if !contains(validOpts, key) {
			return false
		}
	}
	return true
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
