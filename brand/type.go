package brand

import (
	"encoding/xml"
)

type Brand struct {
	XMLName         xml.Name   `xml:"brand"`
	Name            string     `xml:"name,attr"`
	Modname         string     `xml:"modname"`
	Init            string     `xml:"initname"`
	LoginCmd        string     `xml:"login_cmd"`
	ForcedLoginCmd  string     `xml:"forced_login_cmd"`
	UserCmd         string     `xml:"user_cmd"`
	Install         string     `xml:"install"`
	InstallOpts     string     `xml:"install_opts"`
	Boot            string     `xml:"boot"`
	SysBoot         string     `xml:"sysboot"`
	Halt            string     `xml:"halt"`
	VerifyCfg       string     `xml:"verify_cfg"`
	VerifyAdm       string     `xml:"verify_adm"`
	Postclone       string     `xml:"postclone"`
	PostInstall     string     `xml:"postinstall"`
	Attach          string     `xml:"attach"`
	Detach          string     `xml:"detach"`
	Clone           string     `xml:"clone"`
	Uninstall       string     `xml:"uninstall"`
	PreStateChange  string     `xml:"prestatechange"`
	PostStateChange string     `xml:"poststatechange"`
	PreUninstall    string     `xml:"preuninstall,omitempty"`
	Query           string     `xml:"query"`
	Privileges      Privileges `xml:"privilege"`
}

type Platform struct {
	Name             string    `xml:"name,attr"`
	AllowExclusiveIp string    `xml:"allow-exclusive-ip,attr"`
	GlobalMounts     []Mount   `xml:"global_mount"`
	Mounts           []Mount   `xml:"mount"`
	Devices          []Device  `xml:"device"`
	Symlinks         []Symlink `xml:"symlink"`
}

type Device struct {
	Name   string `xml:"name,attr,omitempty"`
	Match  string `xml:"match,attr"`
	IpType string `xml:"ip-type,attr,omitempty"`
	Arch   string `xml:"arch,attr,omitempty"`
}

type Mount struct {
	Special   string `xml:"special,attr"`
	Directory string `xml:"directory,attr"`
	Type      string `xml:"type,attr"`
	Opt       string `xml:"opt,attr,omitempty"`
}

type Symlink struct {
	Source string `xml:"source,attr"`
	Target string `xml:"target,attr"`
}

type Privilege struct {
	Set    string `xml:"set,attr"`
	Name   string `xml:"name,attr"`
	IpType string `xml:"ip_type,attr,omitempty"`
}

type Privileges []Privilege
