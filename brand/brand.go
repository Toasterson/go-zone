package brand

import (
	"encoding/xml"
	"io/ioutil"
	"path/filepath"
)

func GetBrandPath(name string) string {
	return filepath.Join("/usr/lib/brand", name)
}

func LoadBrand(name string) (b *Brand, err error) {
	b = new(Brand)
	content, err := ioutil.ReadFile(filepath.Join(GetBrandPath(name), "config.xml"))
	if err != nil {
		return nil, err
	}
	err = xml.Unmarshal(content, b)
	return b, err
}

func LoadPlatform(name string) (p *Platform, err error) {
	p = new(Platform)
	content, err := ioutil.ReadFile(filepath.Join(GetBrandPath(name), "platform.xml"))
	if err != nil {
		return nil, err
	}
	err = xml.Unmarshal(content, p)
	return p, err
}
