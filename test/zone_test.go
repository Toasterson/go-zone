//+build solaris

package test

import (
	"fmt"
	"testing"

	"git.wegmueller.it/illumos/go-zone"
	"git.wegmueller.it/illumos/go-zone/config"
	"git.wegmueller.it/illumos/go-zone/lifecycle"
)

func TestList(t *testing.T) {
	var hasGlobal = false
	zones, err := zone.List()
	if err != nil {
		t.Fatalf("Got error when getting zones: %s\n", err)
	}
	for pos, val := range zones {
		if val == nil {
			t.Fatalf("got nil zone at pos %d", pos)
		} else {
			if val.Id == 0 {
				hasGlobal = true
			}
			fmt.Printf("Have id: %d, name: %v\n", val.Id, val.Name)
		}
	}
	if !hasGlobal {
		t.Fatalf("Global Zone not in List have %v\n", zones)
	}
}

func TestGetZoneNameById(t *testing.T) {
	name := zone.GetZoneNameById(0)
	if name != "global" {
		t.Fatalf("got name %s instead of global", name)
	}
}

func TestGetAttr(t *testing.T) {
	value := zone.GetAttr(config.GlobalId, config.ZONE_ATTR_ROOT)
	if value != "/" {
		t.Fatalf("got attr %s instead of /", value)
	}
}

func TestGetZoneId(t *testing.T) {
	if zone.GetZoneId() != config.GlobalId {
		t.Fail()
	}
}

func TestZoneInstall(t *testing.T) {
	z := mkZone()
	if err := z.WriteToFile(); err != nil {
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Register(z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	mgr, err := lifecycle.NewManager(z)
	if err != nil {
		t.Fatal(err)
	}
	if err = mgr.Install(nil); err != nil {
		t.Fatal(err)
	}
}

func TestLXZoneInstall(t *testing.T) {
	z := mkLXZone()
	if err := z.WriteToFile(); err != nil {
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Register(z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	mgr, err := lifecycle.NewManager(z)
	if err != nil {
		t.Fatal(err)
	}
	if err = mgr.Install(nil); err != nil {
		t.Fatal(err)
	}
}

func TestBhyveZoneInstall(t *testing.T) {
	z := mkbhyveZone()
	if err := z.WriteToFile(); err != nil {
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Register(z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	mgr, err := lifecycle.NewManager(z)
	if err != nil {
		t.Fatal(err)
	}
	if err = mgr.Install(nil); err != nil {
		t.Fatal(err)
	}
}

