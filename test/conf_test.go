package test

import (
	"fmt"
	"git.wegmueller.it/illumos/go-zone/config"
	"strings"
	"testing"
)

const sampleXml = `<zone name="openvpn" zonepath="/zones/openvpn" autoboot="false" brand="ipkg" ip-type="exclusive" limitpriv="default,priv_net_rawaccess">
	<network physical="vnic0"></network>
	<device match="/dev/lockstat"></device>
	<device match="/dev/tun*"></device>
</zone>
`

const sampleXml2 = `<zone name="template" zonepath="/zones/template" autoboot="false" brand="nlipkg" ip-type="shared">
    <rctl name="zone.cpu-shares">
        <rctl-value priv="privileged" limit="8000" action="none"></rctl-value>
    </rctl>
    <rctl name="zone.cpu-cap">
        <rctl-value priv="privileged" limit="100" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-swap">
        <rctl-value priv="privileged" limit="10737418240" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-locked-memory">
        <rctl-value priv="privileged" limit="5368709120" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-shm-memory">
        <rctl-value priv="privileged" limit="5368709120" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-shm-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-sem-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-msg-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <mcap physcap="10737418240"></mcap>
</zone>`

const sampleXml3 = `<zone name="template" zonepath="/zones/template" autoboot="false" brand="nlipkg" ip-type="shared">
    <rctl name="zone.max-swap">
        <rctl-value priv="privileged" limit="10737418240" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-locked-memory">
        <rctl-value priv="privileged" limit="5368709120" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-shm-memory">
        <rctl-value priv="privileged" limit="5368709120" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-shm-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-sem-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <rctl name="zone.max-msg-ids">
        <rctl-value priv="privileged" limit="5000" action="deny"></rctl-value>
    </rctl>
    <pset ncpu_min="1" ncpu_max="1"></pset>
    <mcap physcap="10737418240"></mcap>
</zone>`

func TestRctl1(t *testing.T) {
	z := config.New("template")
	z.Zonepath = "/zones/template"
	z.Brand = "nlipkg"
	z.SetCPUShares("8000")
	z.SetCappedCPU(1)
	z.SetMaxSwap("10G")
	z.SetMaxLockedMemory("5G")
	z.CappedMemory = config.NewMemoryCap("10G")
	z.SetMaxShmMemory("5G")
	z.SetMaxShmIds("5000")
	z.SetMaxSemIds("5000")
	z.SetMaxMsgIds("5000")
	buff, err := z.Write()
	expected := strings.Replace(sampleXml2, " ", "", -1)
	result := strings.Replace(string(buff), " ", "", -1)
	result = strings.Replace(result, "\t", "", -1)
	if err != nil {
		t.Fatal(err)
	}
	if expected != result {
		t.Fatalf("expected:\n%v\n---\ngotten:\n%v", expected, result)
	}
}

func TestRctl2(t *testing.T) {
	z := config.New("template")
	z.Zonepath = "/zones/template"
	z.Brand = "nlipkg"
	z.SetMaxSwap("10G")
	z.SetMaxLockedMemory("5G")
	z.CappedMemory = config.NewMemoryCap("10G")
	z.SetMaxShmMemory("5G")
	z.SetMaxShmIds("5000")
	z.SetMaxSemIds("5000")
	z.SetMaxMsgIds("5000")
	z.DedicatedCpu, _ = config.NewDedicatedCPU("1")
	buff, err := z.Write()
	expected := strings.Replace(sampleXml3, " ", "", -1)
	result := strings.Replace(string(buff), " ", "", -1)
	result = strings.Replace(result, "\t", "", -1)
	if err != nil {
		t.Fatal(err)
	}
	if expected != result {
		t.Fatalf("expected:\n%v\n---\ngotten:\n%v", expected, result)
	}
}

func TestZone_Read_and_Write(t *testing.T) {
	z := config.New("openvpn")
	if err := z.Read([]byte(sampleXml)); err != nil {
		t.Fatal(err)
	}
	if z.Privileges[0] != "default" {
		t.Fatal("first Privilege not correct")
	}
	if z.IpType != config.IpTypeExclusive {
		t.Fatalf("iptype is wrong wanted const of exclusive got %d", z.IpType)
	}
	if z.Autoboot {
		t.Fatal("expecting autoboot to be false")
	}
	blob, err := z.Write()
	if err != nil {
		t.Fatal(err)
	}
	if string(blob) != sampleXml {
		t.Fatalf("The Two strings should match expexted:\n %s \n gotten: \n %s\n", sampleXml, string(blob))
	}
}

func TestZone_WriteToFile(t *testing.T) {
	z := config.New("testing")
	z.Zonepath = "/zones/testing"
}

func mkZone() *config.Zone {
	z := config.New(testingZoneName)
	z.Autoboot = true
	z.Brand = "nlipkg"
	z.Zonepath = fmt.Sprintf("/zones/%s", testingZoneName)
	z.Networks = []config.Network{
		{Address: "192.168.100.254/24", Physical: "bge0", Defrouter: "192.168.100.1"},
	}
	return z
}

func mkLXZone() *config.Zone {
	z := config.New(testingLXbrandZoneName)
	z.Autoboot = true
	z.Brand = "lx"
	z.Zonepath = fmt.Sprintf("/zcage/vms/%s", testingLXbrandZoneName)
	// Lx branded zones need an image to be created
	// such image could be a zss.gz or tar.gz file
	// image should be specified as an attribute.
	z.Attributes = []config.Attribute{{Name: "img", Type: "string", Value: "/zcage/images/19aa3328-0025-11e7-a19a-c39077bfd4cf.zss.gz"}}
	z.Networks = []config.Network{
		{Address: "192.168.100.254/24", Physical: "vnic6", Defrouter: "192.168.100.1"},
	}
	return z
}

func mkbhyveZone() *config.Zone {
	z := config.New(testingbhyvebrandZoneName)
	z.Brand = "bhyve"
	z.IpType = 2 
	z.Zonepath = fmt.Sprintf("/zcage/vms/%s", testingbhyvebrandZoneName)
	z.Devices = []config.Device{{Match: "/dev/zvol/rdsk/rpool/b0"}}
	z.Attributes = []config.Attribute{{Name: "bootdisk", Type: "string", Value: "rpool/b0"},
		{Name: "cdrom", Type: "string", Value: "/home/cneira/test.iso"}}
	z.FileSystems = []config.FileSystem{
		{Dir: "/home/cneira/test.iso",
			Special: "/home/cneira/test.iso", Type: "lofs",
			Fsoption: []config.FSoption{{Name: "ro"},
				{Name: "nodevices"}}}}
	z.Networks = []config.Network{
		{ AllowedAddress: "192.168.100.254/24", Physical: "vnic6", Defrouter: "192.168.100.1"},
	}
	return z
}
