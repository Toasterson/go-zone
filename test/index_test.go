package test

import (
	"git.wegmueller.it/illumos/go-zone/config"
	"os"
	"testing"
)

func TestZoneConfigure(t *testing.T) {
	z := mkZone()
	if err := z.WriteToFile(); err != nil {
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Register(z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Unregister(z); err != nil {
		t.Fatal(err)
	}
}

func TestUpdate(t *testing.T) {
	z := mkZone()
	if err := z.WriteToFile(); err != nil {
		z.RemoveFile()
		t.Fatal(err)
	}
	if err := config.Register(z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	z.State = config.ZoneStateInstalled
	if err := config.Update(*z); err != nil {
		config.Unregister(z)
		z.RemoveFile()
		t.Fatal(err)
	}
	config.Unregister(z)
	z.RemoveFile()
}

func TestLockUnlockIndex(t *testing.T) {
	var lockFd *os.File
	var err error
	if lockFd, err = config.LockIndex(); err != nil {
		t.Fatal(err)
	}
	config.Unlock(lockFd)
}
