//+build solaris

package zone

import (
	"git.wegmueller.it/illumos/go-zone/config"
)

func lookupZoneInfo(id config.ZoneId_T) (zone *config.Zone, err error) {
	zone = config.New(GetZoneNameById(id))
	zone.Id = id
	err = zone.ReadFromFile()
	return zone, err
}

func List() ([]*config.Zone, error) {
	ids, err := list()
	if err != nil {
		return nil, err
	}
	retVal := make([]*config.Zone, len(ids))
	for i, id := range ids {
		if zone, err := lookupZoneInfo(id); err == nil {
			retVal[i] = zone
		} else {
			return nil, err
		}
	}
	return retVal, nil
}
